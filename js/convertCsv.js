'use strict';

const csv = require('csv-parser');
const fs = require('fs');

let list = [];
const outputFolder = './output/';
const outputFile = outputFolder + 'stolpersteineJSON' + new Date().toISOString() + '.json';


/*
* CSV PARSER LIB
* Anwendung:
* node js/convertCsv.js >> output/{name für die Datei}
* */
let stream = csv({
    raw: false,     // do not decode to utf-8 strings
    separator: ',', // specify optional cell separator
    quote: '',     // specify optional quote character
    escape: '',    // specify optional escape character (defaults to quote value)
    newline: '',  // specify a newline character
    headers: ['Vorname', 'Geburtsname', 'Nachname', 'Geburtstag', 'Adresse', 'Ortsteil', 'Deportationstag', 'Deportationsziel', 'Deportationstag2', 'Deportationsziel2', 'Deportationstag3', 'Deportationsziel3', 'Todestag', 'Todesort'] // Specifing the headers
});

fs.createReadStream('./files/stolpersteine.csv')
    .pipe(stream)
    .on('data', function (data) {

        list.push(data)
    }).on('end', () => {
    fs.mkdir(outputFolder, (err) => {
        //if (err) throw err;
        fs.writeFile(outputFile, JSON.stringify(list, null, 2), (err) => {
            if (err) throw err;
            console.log(`Result has been saved at '${outputFile}'.`);
        });
    });
});

<?php

//Header einfügen
include 'partials/header.php';

?>

<?php
//CSV Datei in Variabel speichern
$csv_datei = "files/stolpersteine.csv";

$felder_trenner = ",";
$zeilen_trenner = "\n";


//Prüfen ob die Datei existiert ansonsten Fehlermeldung
if (@file_exists($csv_datei) == false) {

    echo 'Die CSV Datei: '. $csv_datei.' gibt es nicht!' . "<br>";

} else {

    //Inhalt der Datei in Variabel speichern
    $datei_inhalt = @file_get_contents($csv_datei);


    //Array mit Inhalt nach Zeilen getrennt
    $zeilen = explode($zeilen_trenner,
        $datei_inhalt);

    //Ermittelt die Anzahl der Zeilen der CSV
    $anzahl_zeilen = count($zeilen);


    /*if (is_array($zeilen) == true) {
        echo 'zeilen werden aufgeteilt in felder: ';
        //Alle Zeilen durchlaufen
        foreach($zeilen as $zeile) {
            $felder = explode($felder_trenner, $zeile);

            $i = 0;
            if (is_array($felder) == true) {
                foreach($felder as $felde) {
                    if ($felde != '') {
                        echo (($i != 0) ? ', ':
                                ''). str_replace('"',
                                ' LEER, ', $felde);
                        echo '<br><br>';
                        $i++;
                    }
                }
            }
        }
    }*/
}

$anzahlStolpersteine = $anzahl_zeilen-1;

?>


<div class="intro">
    <img class="intro--pic" src="img/st_grafik.png" alt="stolperstein-blank">
    <div class="intro--text">
        <div class="intro--text__valign">
            <h1>Projekt</h1>
            <h2>Stolperstein</h2>
            <p>
                <?php echo 'Eine Übersicht über ' . $anzahlStolpersteine .' Stolpersteine in Berlin.<br>'; ?>
            </p>
            <button id="scrollMap">explore</button>
        </div>
    </div>
</div>

<div class="stumbling-map">
    <div id="mapid" name="mapip" class="mapid"></div>
</div>


<?php include 'partials/footer.php'; ?>
<?php include 'partials/getlatlng.php'; ?>

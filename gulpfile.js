var gulp = require('gulp');
var sass = require('gulp-sass');

// to use: gulp sass
gulp.task('sass', function(){
    return gulp.src('styles/scss/**/*.scss')
        .pipe(sass()) // Using gulp-sass
        .pipe(gulp.dest('styles/dist/'))
});


// to use: gulp watch
gulp.task('watch', function(){
    gulp.watch('styles/scss/**/*.scss', ['sass']);
});
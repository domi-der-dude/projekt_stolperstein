<?php

//Verbindung mit Datenbank herstellen
require_once ('konfi.php');
$db_link = mysqli_connect (
    MYSQL_HOST,
    MYSQL_BENUTZER,
    MYSQL_KENNWORT,
    MYSQL_DATENBANK
);

mysqli_set_charset($db_link, 'utf8');

//Falls Verbindung fehlschlägt ERROR ausgeben
if (!$db_link )
{
    die('keine Verbindung möglich: ' . mysqli_error());
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Projekt Stolperstein</title>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin=""/>
    <link type="text/css" rel="stylesheet" href="styles/dist/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,900i" rel="stylesheet">

</head>
<body>

<header class="header">
    <div class="logo"><span class="logo--projekt">projekt</span><span class="logo--stolperstein">stolperstein</span></div>
    <ul class="navigation">
        <li><a href="#">Start</a></li>
        <li><a href="#">Info</a></li>
        <li><a href="#">Impressum</a></li>
    </ul>
</header>
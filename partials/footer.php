
<footer>
    Made by Natascha & Dominik
</footer>

<script>
    //Ort in Variabel festlegen
    var mymap = L.map('mapid').setView([52.517547, 13.453995], 12);

    var long1 = '52.517547';
    var lat1 = '13.453995';
    var long2 = '52.515030';
    var lat2 = '13.453995';

    //Karte an bestimmtem Ort anzeigen
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        id: 'mapbox.streets'
    }).addTo(mymap);

    //Marker hinzufügen
    L.marker([long1, lat1]).addTo(mymap).bindPopup("<b>Stolperstein #1</b><br>Albertz, Schweizer.").openPopup();
    L.marker([long2, lat2]).addTo(mymap).bindPopup("<b>Stolperstein #2</b><br>Albertz, Schweizer.").openPopup();

</script>


</body>
</html>